"""
SPDX-License-Identifier: MIT
Copyright (c) 2020-2022 XVM Contributors
"""

#
# Imports
#

# cpython
import logging


# xfw.native
from xfw_native.python import XFWNativeModuleWrapper



#
# Globals
#

g_xfw_ping = None



#
# Public
#

def ping(address):
    global g_xfw_ping
    if not g_xfw_ping:
        return None
    return g_xfw_ping.ping(address)



#
# XFW Loader
#

def xfw_is_module_loaded():
    global g_xfw_ping
    return g_xfw_ping is not None


def xfw_module_init():
    global g_xfw_ping
    g_xfw_ping = XFWNativeModuleWrapper('com.modxvm.xfw.ping', 'xfw_ping.pyd', 'XFW_Ping')


def xfw_module_fini():
    global g_xfw_ping
    g_xfw_ping = None

