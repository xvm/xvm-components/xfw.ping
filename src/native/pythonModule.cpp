// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2022 XVM Contributors

// pybind11
#include <pybind11/pybind11.h>

// xfw.ping
#include "ping.h"


//
// Module
//

PYBIND11_MODULE(XFW_Ping, m) {
    m.doc() = "XFW Ping module";
    m.def("ping", &ping, pybind11::call_guard<pybind11::gil_scoped_release>());
}
