// SPDX-License-Identifier: MIT
// Copyright (c) 2020-2022 XVM Contributors

#pragma once

#include <string>

int ping(const std::string& address);
